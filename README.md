# linux-enhancments

[[_TOC_]]


## About

This page is a living document and I will update it, but they will be
infrequent updates. As IMHO, Linux is a 99% solution for those looking for
desktop freedom. Having said that, if you know of real solutions to these
problems please reach out.

This page is started as a way to vent my frustrations on the state of linux
in 2023.  And to be fair, this is not really about linux as much as it is
on desktop linux. Now I am not calling out any specific version of desktop
linux, but speaking in generic terms, as I feel these issues are across all
desktop versions.

In 2023, we are still dealing with many short comings and this is my way of
relaying that frustration. Having said that, if you...


## Notes
- If you are about to take the linux journey and found this page. Do
  not let this page stop you. I have been using linux as my daily driver since
  2006 and have not looked back. Linux can replace much of what windows and
  mac can do.

- All these frustrations can be solve with either a VM or an app running
  in wine, that is not the purpose of this page. This page is to document
  linux native solutions


## Frustration Grading

- Low
   - Minor push for linux desktop adoption

- Medium
   - Medium push for linux desktop adoption

- High
   - Major push for linux desktop adoption


## Office Frustrations


### PDF and Smart Card Signing (FG: **low**)

Yes, still a problem in 2023. One of the times I need to use windows is to
sign PDFs with my smart card. It's not that linux cannot read my smart card
as I use it to access certain web sites. The problem is the lack of the PDF
software vendors to support this.

**Update 2024-08-10**
- [Qoppa PDF Studio](https://www.qoppa.com/pdfstudio) has been shown to support
  this. While I have not played yet with the solution too much, I will be
  investigating this. It will take time as I am very busy and only need this
  solution every now and then. 


### Word (docx) support (FG: **high**)

I think there has been a ton of work in this area, and I am not talking web
based solutions. Purely desktop. And don't get me wrong, for the most part
collaborating between LibreOffice and Word is awesome. But then there is
the 1%.

But why LibreOffice, well because I like out-of-the-box solutions and
LibreOffice is there by default with many linux distributions.

So what's the issue, well I work with a custumer who does a ton of "under
the hood" stuff in Micro$oft Office, specifically word (docx) files. This
is where collaboration breaks down when using the holy grail office suite
LibreOffice. When opening that kind of document, it renders like garbage
and trying to make changes will harm the Word people from being able to work
with the doc.

I know this is a huge tasking, but I do wish that the LibreOffice folks would
do more to support this. Perhaps get together with the OnlyOffice people to
work on a shared library? Just a thought, see the note.

#### Notes

- Since 2020, I have been able to work around this with the
[OnlyOffice](https://www.onlyoffice.com/en/download-desktop.aspx)
desktop suite. But now I have two office suites on my desktop. Not really
complaining because a solution is a solution, but it would be nice to only
need libreoffice.

- I recognize there are other office suites for linux. I have not tried them
out in reference to the "under the hood" issue described above and OnlyOffice
has resolved this.

## Considerations

### Contributing

If you are open to sharing some of your frustrations, please reach out
and we'll consider added them to this repo.  I feel stringly that finding
appropriate solutions to the 1% will certainly make linux desktop adoption
better.

### Authors and acknowledgment
- Currently Myself

